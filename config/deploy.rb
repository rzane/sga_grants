require 'rvm/capistrano'
require 'bundler/capistrano'

set :application, "sga_grants"
set :scm, :git
set :repository,  "git@github.com:rzane/sga_grants.git"
set :user, "ray"
set :use_sudo, false
set :deploy_to, "/var/www/sga_grants"
set :deploy_via, :remote_cache
set :normalize_asset_timestamps, false

set :rvm_ruby_string, "ruby-2.0.0-p195"

set :rails_env, "production"

default_run_options[:shell] = false

unless ENV['GRANTS_HOST']
  message = <<-EOS
    You need to provide the hostname of the server you'd like to deploy to.
    Example:
    \tGRANTS_HOST=example.com bundle exec cap deploy
  EOS
  abort message
end

role :web, ENV['GRANTS_HOST']
role :app, ENV['GRANTS_HOST']
role :db,  ENV['GRANTS_HOST'], :primary => true

after "deploy:finalize_update", "deploy:symlink_db_config"
after "deploy", "deploy:migrate"
#after "deploy:migrate", "deploy:seed"
after "deploy:migrate", "deploy:cleanup"

namespace :deploy do
	task :seed do
		run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
	end

	task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
  	run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
 
  task :symlink_db_config, roles: :app do
    run "ln -nfs #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
  end
end
