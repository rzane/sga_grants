mailer_type = APP_CONFIG["mailer_type"].to_sym
ActionMailer::Base.delivery_method = mailer_type

ActionMailer::Base.smtp_settings = APP_CONFIG["smtp_settings"] if  mailer_type == :smtp
