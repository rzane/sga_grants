SimpleForm.setup do |config|
  config.boolean_style = :nested
  config.button_class = 'btn'
  config.error_notification_tag = :div
  config.error_notification_class = 'alert alert-error'
  config.label_class = 'col-lg-2 control-label'
  config.browser_validations = false
end

SimpleForm.setup do |config|
  # you need an updated simple_form gem for this to work, I'm referring to the git repo in my Gemfile
  config.input_class = "form-control"
  config.label_class = 'col-lg-2 control-label'

  config.wrappers :bootstrap, tag: 'div', class: 'form-group', error_class: 'has-error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.wrapper tag: 'div', class: 'col-lg-10' do |ba|
      ba.use :input #, wrap_with: { tag: 'div', class: 'col-lg-10'}
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end

  config.wrappers :group, tag: 'div', class: "form-group", error_class: 'error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label
    b.use :input, wrap_with: { :class => "input-group" }
    b.use :hint,  wrap_with: { tag: 'span', :class => 'help-block' }
    b.use :error, wrap_with: { tag: 'span', :class => 'help-inline' }
  end

  # Wrappers for forms and inputs using the Twitter Bootstrap toolkit.
  # Check the Bootstrap docs (http://twitter.github.com/bootstrap)
  # to learn about the different styles for forms and inputs,
  # buttons and other elements.
  config.default_wrapper = :bootstrap
end
