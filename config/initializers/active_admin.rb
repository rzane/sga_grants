module ActiveAdmin
  class Comment < ActiveRecord::Base
    attr_accessible :body, :namespace, :resource_id, :resource_type
  end
end

ActiveAdmin.setup do |config|
  config.site_title = "SGA Grants"

  config.current_user_method = :current_user
  config.logout_link_path = :destroy_user_session_path
  config.logout_link_method = :delete
  config.on_unauthorized_access = :access_denied

  config.site_title_link = "/"

  config.authentication_method = :authenticate_user!
  config.authorization_adapter = ActiveAdmin::CanCanAdapter

  config.batch_actions = true

  config.namespace :admin do |admin|
   admin.build_menu :default do |menu|
     menu.add label: "Organizations", priority: 1
     menu.add label: "Applications", priority: 2
     menu.add label: "Settings", priority: 4
   end
  end
end
