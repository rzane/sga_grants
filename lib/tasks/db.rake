file "config/database.yml.example" do |task|
  abort "I don't know what to tell you. There's no #{task.name}. So maybe you should make one of those, and see where that gets you." unless Rails.env.production?
end

file "config/database.yml" => "config/database.yml.example" do |task|
  unless Rails.env.production?
    puts "I found #{task.prerequisites.first}, so I'll make a copy of it for you."
    cp task.prerequisites.first, task.name
    abort "Make sure it matches your setup, then rerun the last command."
  end
end

task :environment => "config/database.yml"
