class ApplicationsDatatable
  delegate :params, :current_user, :can?, :number_to_currency, :content_tag, :link_to, :application_path, :edit_application_path, to: :@view
  
  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {records: data, queryRecordCount: total_count}
  end

  private

  def data
    @data ||= collection.map {|object| object_fields(object) }
  end

  def total_count
    @total_count ||= collection.total_count
  end

  def collection
    @collection ||= fetch_collection
  end

  def pending_fields app
    {
      organization: link_to(app.organization.name, application_path(app)),
      amountRequested: number_to_currency(app.amt_requested),
      neededBy: app.funds_needed_by.try(:to_formatted_s),
      dateOfVote: app.date_of_vote.try(:to_formatted_s) || content_tag(:i, "Unassigned"),
      created: app.created_at.try(:to_formatted_s)
    }
  end

  def archived_fields app
    label_class = app.vote_result == "approved" ? 'label-success' : 'label-danger'
    archived = {
      approvedAmount: app.final_amount ? number_to_currency(app.final_amount) : content_tag(:i, "Unassigned"),
      voteResult: content_tag(:span, app.vote_result.try(:titleize), class: "label #{label_class}")
    }
    pending_fields(app).merge(archived)
  end

  def incomplete_fields app
    actions = []
    actions << link_to("Resume", edit_application_path(app)) if app.applicant_id.eql? current_user.id
    actions << link_to("Delete", application_path(app), method: :delete, data: { confirm: "Are you sure?" })
    {
      organization: link_to(app.organization.name, application_path(app)),
      amountRequested: number_to_currency(app.amt_requested),
      created: app.created_at.try(:to_formatted_s),
      actions: actions.join(' | ')
    }
  end

  def object_fields(app)
    send("#{query_type}_fields", app)
  end

  def sort_by
    params[:sorts].map { |k,v| "#{k} #{v.to_i < 0 ? "desc" : "asc"}" }.join(', ')
  end

  def search_query
    ["lower(#{Organization.table_name}.name) like ?", "%#{params[:queries][:search].downcase}%"]
  end

  def query_type
    types = %w{ pending archived incomplete }
    return "pending" unless params[:type] && types.include?(params[:type])
    params[:type]
  end

  def fetch_collection
    @applications = Application.joins(:organization).includes(:organization, :expenditures, {event: [:transportation, :hotel]})
    @applications = @applications.where(applicant_id: current_user.id) unless can?(:view_all, Application)
    @applications = @applications.order(sort_by) if params[:sorts]
    @applications = @applications.where(search_query) if params[:queries]
    @applications = @applications.send(query_type).page(params[:page]).per(params[:perPage])
  end
end
