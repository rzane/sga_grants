class Event < ActiveRecord::Base
  attr_accessible :hotel_attributes, :transportation_attributes, :application_id, :end_date, :number_attending, :place_of_event, :start_date

  belongs_to :application
  has_one :hotel
  has_one :transportation

  validates :number_attending, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :place_of_event, presence: true
  validates_date :start_date, :on_or_after => lambda{ Date.today}
  validates_date :end_date, :on_or_after => :start_date
  validates_date :start_date, :before => lambda { (Chronic.parse("next May").beginning_of_month + 14.days).to_date }

  accepts_nested_attributes_for :hotel, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :transportation, :reject_if => :all_blank, :allow_destroy => true
end
