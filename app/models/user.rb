class User < ActiveRecord::Base
  devise :cas_authenticatable

  # In order or permission!
  ROLES = %w{user org_leader sga_member treasurer admin}

  # Setup accessible (or protected) attributes for your model
  attr_accessible :username, :email, :role, :fname, :lname

  has_many :organization_users
  has_many :organizations, :through => :organization_users
  has_many :applications, :foreign_key => :applicant_id

  has_many :updated_organizations, :class_name => "Organization", :foreign_key => :updated_by

  before_save :set_defaults

	validates :fname, presence: true, :on => :update
  validates :lname, presence: true, :on => :update
  validates :email, presence: true, email_format: { message: "Invalid Email Address" }, :on => :update

  def role?(role)
    self.role.to_sym == role
  end

  def completed_signup?
    (self.email.nil? || self.fname.nil? || self.lname.nil?) ? false : true
  end

  def set_defaults
    self.role = "user" if self.role.nil?
    self.email ||= "#{self.username}@mix.wvu.edu"
  end

  def fullname
    "#{self.fname} #{self.lname}".try(:titleize)
  end

  def role_greater_than_or_equal_to?(role)
    self.role ? (ROLES.index(self.role) >= ROLES.index(role.to_s)) : false
  end
end
