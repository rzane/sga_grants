class TransportationType < ActiveRecord::Base
  attr_accessible :name
  has_many :transportations
end
