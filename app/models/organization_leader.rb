class OrganizationLeader < ActiveRecord::Base
	attr_accessible :fname, :lname, :email, :title, :organization_id
	belongs_to :organization
	has_many :verified_applications, :class_name => "Application", :foreign_key => :verified_by_id

  validates_presence_of :fname, :lname, :email, :title

	def fullname
		"#{fname} #{lname}"
	end
end
