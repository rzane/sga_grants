class FundraisingHistory < ActiveRecord::Base
  attr_accessible :amount_received, :date, :application_id, :name, :organization_id, :confirmed

  belongs_to :organization

  validates :date, :presence => true
  validates :amount_received, :presence => true, numericality: { greater_than: 0 }
  validates :name, :presence => true

  def self.create_from_successful_grant app
    create(
      organization_id: app.organization.id,
      name: "SGA Grant",
      date: Date.today,
      amount_received: app.final_amount
    )
  end
end
