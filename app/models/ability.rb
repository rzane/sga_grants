class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    can [:index, :show], Organization

    if user.role_greater_than_or_equal_to?(:admin)
      can :manage, :all
      can :read, ActiveAdmin::Page, :name => "Dashboard"
    end

    if user.role_greater_than_or_equal_to?(:treasurer)
      can :governor_update, Application
    end

    if user.role_greater_than_or_equal_to?(:sga_member)
      can :create, Organization
      can :view_comments, Organization
      can [:view_all, :finance_report], Application
      can :edit_on_show_page, Organization
    end
    
    if user.role_greater_than_or_equal_to?(:user)
      can :update, Organization
      can [:create, :index, :show], Application
      can [:update, :show], User, :id => user.id
      can [:update, :destroy], Application, :complete => false, :applicant_id => user.id
    end
  end
end
