class Transportation < ActiveRecord::Base
  attr_accessible :additional_info, :company_name, :cost, :phone, :transportation_type_id, :website

  belongs_to :event
  belongs_to :transportation_type
  validates :company_name, presence: true
  validates :cost, numericality: { greater_than: 0 }
  validates :phone, presence: true
  validates :website, presence: true
end
