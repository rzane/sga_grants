class Hotel < ActiveRecord::Base
  attr_accessible :cost_per_room, :name, :people_per_room, :phone, :city, :state, :number_of_rooms, :additional_info
  belongs_to :event

  validates :cost_per_room, numericality: { greater_than: 0 }
  validates :cost_per_room, presence: true
  validates :name, presence: true
  validates :people_per_room, numericality: { only_integer: true, greater_than: 0 }
  validates :people_per_room, presence: true
  validates :phone, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :number_of_rooms, numericality: { only_integer: true, greater_than: 0 }
  validates :number_of_rooms, presence: true

  def total_cost
    self.cost_per_room * self.number_of_rooms
  end
end
