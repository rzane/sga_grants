class Organization < ActiveRecord::Base
  attr_accessible :description, :category_ids, :primary_category_id, :website, :meeting_day_of_week,
    :meeting_frequency, :meeting_time, :name, :number_of_members, :purpose, :updated_by,
    :fundraising_histories_attributes, :organization_leaders_attributes
  has_many :applications

  belongs_to :updated_by, class_name: "User", :foreign_key => :updated_by

  has_many :category_organizations
  has_many :categories, :through => :category_organizations

  has_many :active_admin_comments, as: :resource
  
  belongs_to :primary_category, :class_name => "Category", :foreign_key => :primary_category_id

  has_many :organization_users
  has_many :organization_leaders

  has_many :users, :through => :organization_users
  has_many :fundraising_histories

  accepts_nested_attributes_for :fundraising_histories, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :organization_leaders, reject_if: :all_blank, allow_destroy: true

  # Define fields that don't need to be filled to create an application without updating org
  # ex. [:number_of_members, :purpose]
  APPLICATION_OPTIONAL_FIELDS = []
  MEETING_FREQUENCIES = {
    0 => "Weekly",
    1 => "Bi-Weekly",
    2 => "Monthly",
    3 => "Semester",
    4 => "Yearly"
  }


  def self.search query
    where("lower(#{Organization.table_name}.name) like ?", "%#{query.downcase}%")
  end

  def self.grouped_by_category
    order("#{Organization.table_name}.name ASC").group_by{|o| o.primary_category.name}.sort
  end

  def self.include_categories_and_leaders
    joins(:categories).includes(:categories, :organization_leaders, :primary_category)
  end

  def category_name
    self.category.name
  end

  def word_meeting_frequency
    MEETING_FREQUENCIES[self.meeting_frequency]
  end

  validates :description, presence: true
  validates :meeting_day_of_week, presence: true
  validates :meeting_frequency, presence: true
  validates :meeting_time, presence: true
  validates :name, presence: true
  validates :number_of_members, presence: true
  validates :number_of_members, numericality: { only_integer: true, greater_than: 0 }
  validates :purpose, presence: true
  validates :primary_category, presence: true
  validates :categories, length: {minimum: 1, message: "You must select at least one category."}

  before_validation :ensure_primary_cat_in_categories

  def ensure_primary_cat_in_categories
    return unless primary_category

    unless categories.include? primary_category
      self.categories << primary_category
    end
  end
end
