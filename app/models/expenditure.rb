class Expenditure < ActiveRecord::Base
  attr_accessible :application_id, :cost, :description, :expenditure_type_id

  belongs_to :application
  belongs_to :expenditure_type

  validates :cost, presence: true, numericality: { greater_than: 0 }
  validates :expenditure_type_id, presence: true
end
