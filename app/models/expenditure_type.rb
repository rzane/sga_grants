class ExpenditureType < ActiveRecord::Base
  attr_accessible :name
  has_many :expenditures
end
