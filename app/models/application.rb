class Application < ActiveRecord::Base
  attr_accessible :verified, :verified_by, :verified_by_id,
    :final_amount, :paid_amount, :paid_in_full, :organization_attributes,
    :complete, :expenditures_attributes, :event_attributes, :additional_info,
    :applicant_id, :date_of_vote, :funds_needed_by, :impact_on_univ, :organization_id,
    :reason_for_funds, :vote_result

  belongs_to :organization
  belongs_to :applicant, class_name: "User"
  belongs_to :verified_by, class_name: "OrganizationLeader", foreign_key: :verified_by_id
  has_many :expenditures
  has_one :event

  STEPS = %w[ application event hotel transportation expenditures fundraising_history ]

  accepts_nested_attributes_for :organization
  accepts_nested_attributes_for :expenditures, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :event, reject_if: :all_blank

  after_initialize :set_defaults

  validates :applicant_id, presence: true
  validates :final_amount, numericality: { greater_than: 0 }, allow_blank: true
  validates :final_amount, presence: true, if: :should_confirm_final_amount?
  validates :impact_on_univ, presence: true
  validates :reason_for_funds, presence: true
  validates :vote_result, presence: true
  validates_date :date_of_vote, on_or_after: lambda{ Date.today }, allow_blank: true, if: :requires_date_of_vote?
  validates_date :funds_needed_by, on_or_after: lambda{ Date.today }, on: :create
  validates_date :funds_needed_by, before: lambda { (Chronic.parse("next May").beginning_of_month + 14.days).to_date }, on: :create

  ARCHIVED_VOTE_RESULTS = ['approved', 'denied']

  class << self
    def include_all_associations
      organization_includes = [:fundraising_histories, :primary_category, :categories, :organization_leaders]
      joins(:organization).includes(event: [:hotel, :transportation], organization: organization_includes)
    end

    def final_amount_sum
      archived
      .where(vote_result: "approved")
      .where("date_of_vote > ?", Chronic.parse("last June").beginning_of_month)
      .sum("final_amount")
    end

    def pending
      where(vote_result: "pending", complete: true)
    end

    def incomplete
      where(complete: false)
    end

    def archived
      where(vote_result: ARCHIVED_VOTE_RESULTS, complete: true)
    end
  end

  def pending?
    complete == true && vote_result == "pending"
  end

  def incomplete?
    complete == false
  end

  def archived?
    ARCHIVED_VOTE_RESULTS.include?(vote_result) && complete == true
  end

  def notify_date_of_vote_assigned
    ApplicationMailer.date_of_vote_assigned(applicant, self).deliver
  end

  def notify_approved
    FundraisingHistory.create_from_successful_grant self
    ApplicationMailer.application_approved(applicant, self).deliver
  end

  def update_and_notify attributes
    success = update_attributes(attributes)
    notify_date_of_vote_assigned if pending?
    notify_approved if should_confirm_final_amount?
    success
  end

  def should_confirm_final_amount?
    self.vote_result == "approved"
  end

  def set_defaults
    self.vote_result ||= 'pending'
    self.complete = false if self.complete.nil?
    self.verified = false if self.verified.nil?
  end

  def requires_date_of_vote?
    self.date_of_vote.nil?
  end

  def requires_vote_result?
    self.date_of_vote && self.vote_result == "pending" && Date.today >= self.date_of_vote
  end

  def requires_governor_attention?
    self.requires_date_of_vote? || self.requires_vote_result?
  end

  def incomplete?
    self.complete.nil?
  end

  def amt_requested
    total_cost = self.expenditures.map(&:cost).sum || 0
    if self.event
      total_cost = total_cost + self.event.transportation.cost if self.event.transportation
      total_cost = total_cost + self.event.hotel.total_cost if self.event.hotel
    end
    total_cost
  end

  def governor_message
    if requires_governor_attention?
      message = "You need to assign a "
      message += "date of vote" if requires_date_of_vote?
      message += "vote result" if requires_vote_result?
      message += " to this grant."
    end
  end
end
