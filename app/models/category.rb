class Category < ActiveRecord::Base
  attr_accessible :organizations_attributes, :name

	has_many :category_organizations
  has_many :organizations, :through => :category_organizations

  
	has_many :primary_organizations, :class_name => "Organization", :foreign_key => :primary_category_id

	accepts_nested_attributes_for :organizations
end
