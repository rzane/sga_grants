module ApplicationsHelper
  def formatted_event_fields(event)
    fields = {
      place_of_event: event.place_of_event.try(:titleize),
      number_attending: event.number_attending,
      start_date: event.start_date.try(:to_s, :long),
      end_date: event.end_date.try(:to_s, :long)
    }
    hash_to_list_items(fields)
  end

  def app_updated_response(application)
    { 
      error: false,
      response: "Updated",
      partial: formatted_application_fields(application)
    }
  end

  def number_of_rooms_description(hotel)
    "#{hotel.try(:number_of_rooms)} rooms, #{number_to_currency(hotel.try(:cost_per_room))} per room"
  end

  def formatted_hotel_fields(hotel)
    fields = {
      name: hotel.name.try(:titleize),
      total_cost: number_to_currency(hotel.total_cost),
      cost_per_room: number_to_currency(hotel.cost_per_room),
      number_of_rooms: hotel.number_of_rooms,
      people_per_room: hotel.people_per_room,
      city: hotel.city.try(:titleize),
      state: hotel.state.try(:titleize),
      phone: hotel.phone
    }
    fields[:additional_info] = hotel.additional_info unless hotel.additional_info.empty?
    hash_to_list_items(fields)
  end

  def formatted_transportation_fields(transport)
    fields = {
      type: transport.transportation_type.name,
      company: transport.company_name.titleize,
      cost: number_to_currency(transport.cost),
      website: transport.website,
      phone: transport.phone,
    }
    fields[:additional_info] = transport.additional_info unless transport.additional_info.empty?
    hash_to_list_items(fields)
  end

  def formatted_applicant_fields(applicant)
    {
      applicant_name: applicant.try(:fullname),
      applicant_email: (mail_to(applicant.email) if applicant.present?)
    }
  end

  def meeting_time_fields(f)
    style = (f.object.more_often_than_biweekly? ? "display:none;" : "")
    required = (f.object.more_often_than_biweekly? ? false : true)
    week_days = Time::DAYS_INTO_WEEK.keys.map { |d| d.to_s.titleize }
    [
      f.input(:meeting_day_of_week, prompt: "Day of Week", collection: week_days, required: required, style: style),
      f.input(:meeting_time, as: :timepicker, style: style, required: required)
    ].join('').html_safe
  end

  def formatted_application_fields(app)
    fields = {
      amount_requested: number_to_currency(app.amt_requested),
      amount_awarded: number_to_currency(app.final_amount) || content_tag(:em, "Pending"),
      funds_needed_by: app.funds_needed_by.try(:to_s, :long),
      date_of_vote: (app.date_of_vote.try(:to_s, :long) || content_tag(:em, "Unassigned")),
      vote_result: app.vote_result.titleize,
      reason_for_funds: app.reason_for_funds,
      impact_on_university: app.impact_on_univ,
      additional_info: app.additional_info,
      applied_on: app.created_at.to_date.try(:to_s, :long),
      last_updated: app.updated_at.try(:strftime, "%B %e, %Y at %l:%M %p")
    }
    fields.merge!(formatted_applicant_fields(app.applicant))
    verified_by = app.verified_by ? "#{app.verified_by.try(:title).try(:titleize)} #{app.verified_by.try(:fullname)}" : "Unverified"
    fields[:verified_by] = verified_by
    hash_to_list_items(fields)
  end

  def hash_to_list_items(items)
    haml_tag :table, class: "table table-striped" do
      items.each do |key, value|
        haml_tag :tr do
          haml_tag :td, key.to_s.titleize, class: "row-title"
          haml_tag :td, value
        end
      end
    end
  end

  def link_to_add(type, open, container_selector, destroy_selector)
    phrase_to_use = (open ? 'Remove ' : 'Add ') + type.to_s.titleize
    haml_tag :div, class: "add-one-association" do
      haml_tag :span, class: "add-one-wrap" do
        haml_tag :h4, "Would you like to add #{phrase_to_use.downcase} information?", class: "question", style: (open ? 'display:none;' : '')
        haml_tag :a, phrase_to_use, href: "#", id: "add_#{type.to_s}", class: "slider_button btn btn-default #{'open btn-danger' if open}", data: {name: type.to_s.titleize, container: container_selector, destroy: destroy_selector}
      end
    end
  end

  def get_date_string(value)
    (value || DateTime.now).to_date.to_s
  end

  def us_states
    [
      ['Alabama', 'AL'],
      ['Alaska', 'AK'],
      ['Arizona', 'AZ'],
      ['Arkansas', 'AR'],
      ['California', 'CA'],
      ['Colorado', 'CO'],
      ['Connecticut', 'CT'],
      ['Delaware', 'DE'],
      ['District of Columbia', 'DC'],
      ['Florida', 'FL'],
      ['Georgia', 'GA'],
      ['Hawaii', 'HI'],
      ['Idaho', 'ID'],
      ['Illinois', 'IL'],
      ['Indiana', 'IN'],
      ['Iowa', 'IA'],
      ['Kansas', 'KS'],
      ['Kentucky', 'KY'],
      ['Louisiana', 'LA'],
      ['Maine', 'ME'],
      ['Maryland', 'MD'],
      ['Massachusetts', 'MA'],
      ['Michigan', 'MI'],
      ['Minnesota', 'MN'],
      ['Mississippi', 'MS'],
      ['Missouri', 'MO'],
      ['Montana', 'MT'],
      ['Nebraska', 'NE'],
      ['Nevada', 'NV'],
      ['New Hampshire', 'NH'],
      ['New Jersey', 'NJ'],
      ['New Mexico', 'NM'],
      ['New York', 'NY'],
      ['North Carolina', 'NC'],
      ['North Dakota', 'ND'],
      ['Ohio', 'OH'],
      ['Oklahoma', 'OK'],
      ['Oregon', 'OR'],
      ['Pennsylvania', 'PA'],
      ['Puerto Rico', 'PR'],
      ['Rhode Island', 'RI'],
      ['South Carolina', 'SC'],
      ['South Dakota', 'SD'],
      ['Tennessee', 'TN'],
      ['Texas', 'TX'],
      ['Utah', 'UT'],
      ['Vermont', 'VT'],
      ['Virginia', 'VA'],
      ['Washington', 'WA'],
      ['West Virginia', 'WV'],
      ['Wisconsin', 'WI'],
      ['Wyoming', 'WY']
    ]
  end
end
