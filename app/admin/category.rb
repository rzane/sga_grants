ActiveAdmin.register Category do
  menu :parent => "Organizations"

  index do |categories|
    column("NAME") do |cat|
      link_to cat.name, admin_category_path(cat)
      # link_to organization.name, admin_organization_path
    end
    # organizations.each do |o|
  end

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
    end
    panel "Organizations" do
      table_for category.organizations.order("name asc") do
        column "Name" do |organization|
          link_to organization.name, admin_organization_path(organization)
        end
      end
    end
    active_admin_comments
  end
end
