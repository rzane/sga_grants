ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    div :class => "blank_slate_container", :id => "dashboard_default_message" do
      h1 "Welcome to SGA Grants"
    end

    hr
    br
    columns do
      column do
        panel "Recent Applications" do
          ul do
            Application.joins(:organization).includes(:organization).order("#{Application.table_name}.created_at desc").limit(10).map do |application|
              li link_to("#{application.organization.name} at #{application.created_at.to_s(:short)}", admin_application_path(application))
            end
          end
        end
      end

      column do
        panel "Recent Organization Updates" do
          ul do
            Organization.all.order("updated_at desc").limit(10).map do |org|
              li link_to("#{org.name} at #{org.updated_at.to_s(:short)}", admin_organization_path(org))
            end
          end
        end
      end
    end

    columns do
      column do
        panel "Recent User Sign Ups" do
          ul do
            User.all.order("created_at desc").limit(20).map do |user|
              li link_to("#{user.fullname} at #{user.created_at.to_s(:short)}", admin_user_path(user))
            end
          end
        end
      end
    end

  end # content
end
