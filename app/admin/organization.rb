ActiveAdmin.register Organization do
  menu :parent => "Organizations"


  index do |organizations|
    column "Name" do |org|
      link_to org.name, admin_organization_path(org)
    end
    column :created_at
    column :updated_at
  end


  show do |organization|
    attributes_table do
      row :id
      row :name
      row :meeting_day_of_week
      row :meeting_time
      row :meeting_frequency
      row :description
      row :purpose
      row :number_of_members
      row :website
      row :created_at
      row :updated_at
      row :updated_by
      row :primary_category

    end
    panel "Categories" do
      table_for organization.categories.order("name asc") do
        column "Name" do |category|
          link_to category.name, admin_category_path(category)
        end
      end
    end
    panel "Organization Leaders" do
      table_for organization.organization_leaders do
        column "Name" do |ol|
          link_to ol.fullname, admin_organization_leader_path(ol)
        end
        column "Title" do |ol|
          ol.title.titleize
        end
      end
    end
    panel "Fundraising History" do
      table_for organization.fundraising_histories.order("created_at desc") do
        column "Name" do |fh|
          link_to fh.name, admin_fundraising_history_path(fh)
        end
        column :amount_received
        column :created_at
      end
    end
    active_admin_comments
  end
end
