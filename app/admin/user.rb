ActiveAdmin.register User do
  menu :priority => 3

  index do
    column "Username" do |user|
      link_to user.username, admin_user_path(user)
    end
    column "Email" do |user|
      user.try(:email)
    end
    column "Firstname" do |user|
      user.try(:fname)
    end
    column "Lastname" do |user|
      user.try(:lname)
    end
    column "Role" do |user|
      user.try(:role)
    end
  end
end
