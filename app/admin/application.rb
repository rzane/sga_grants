ActiveAdmin.register Application do
  menu :parent => "Applications"

  index do |applications|
    applications.each do |a|
      column "Application" do |app|
        link_to app.created_at, admin_application_path(app)
      end
      column :organization
      column :applicant
      column "Vote Result" do |app|
        app.vote_result.titleize
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :organization
      row :applicant
      row :complete
      row "Event" do
        link_to application.event.place_of_event, admin_event_path(application.event) if application.event
      end
      row "Hotel" do
        if application.event && application.event.hotel
          link_to application.event.hotel.name, admin_hotel_path(application.event.hotel)
        end
      end
      row "Transportation" do
        if application.event && application.event.transportation
          link_to application.event.transportation.company_name, admin_transportation_path(application.event.transportation)
        end
      end
      row :amt_requested
      row :funds_needed_by
      row :reason_for_funds
      row :impact_on_univ
      row :date_of_vote
      row :vote_result
      row :final_amount
      row :additional_info
      row :created_at
      row :updated_at
    end
    panel "Expenditures" do
      table_for application.expenditures do
        column "Cost" do |e|
          link_to number_to_currency(e.cost), admin_expenditure_path(e)
        end
        column "Type" do |e|
          e.expenditure_type.name
        end
      end
    end
    active_admin_comments
  end
end