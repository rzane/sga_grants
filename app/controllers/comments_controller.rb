class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @organization = Organization.find(params[:organization])
    @comment = @organization.active_admin_comments.build
    @comment.body = params[:active_admin_comment][:body]
    @comment.author_id = current_user.id
    @comment.author_type = "User"
    @comment.namespace = "admin"
    
    if @comment.save
      redirect_to :back, flash: {success: t('comments.created')}
    else
      redirect_to :back, flash: {error: t('comments.error')}
    end

  end
end