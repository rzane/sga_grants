class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :store_location
  before_filter :ensure_signup_completion

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, error: exception.message
  end

  def store_location
    if !(request.fullpath =~ /users/) && !request.xhr?
      session[:previous_url] = request.fullpath
    end
  end

  def access_denied(exception)
    redirect_to root_url
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end

  def ensure_signup_completion
    unless !user_signed_in? || current_user.completed_signup? || (["users", "cas_sessions"].include?(controller_name))
      redirect_to edit_user_path(current_user), flash: {info: t('users.fill_in_info') }
    end
  end

  def notify_incomplete_apps
    if user_signed_in? && current_user.applications.incomplete.any?
      flash.now[:warning] = t('applications.incomplete_app')
    end
  end
end
