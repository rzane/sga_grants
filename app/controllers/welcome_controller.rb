class WelcomeController < ApplicationController
  def index
    @app_count = Application.count
    @org_count = Organization.count
    @cat_count = Category.count
  end
end
