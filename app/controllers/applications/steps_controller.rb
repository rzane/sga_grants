class Applications::StepsController < ApplicationController
  include Wicked::Wizard
  steps :event, :hotel, :transportation, :expenditures, :fundraising_history

  def show
    @application = Application.find(params[:application_id])
    return redirect_to @application if @application.complete?
    @application.build_event unless @application.event
    @application.event.build_transportation unless @application.event.transportation
    @application.event.build_hotel unless @application.event.hotel
    @application.expenditures.build
    render_wizard
  end

  def update
    @application = Application.find(params[:application_id])
    if (step == :fundraising_history) && (!@application.complete?)
      params[:application][:complete] = true
      session[:app_id] = nil
      deliver_emails
    end
    @application.update_attributes(params[:application])
    render_wizard @application
  end

  private

  def deliver_emails
    access_token = ApiKey.create!.access_token
    people_to_update = [@application.applicant]
    @application.organization.organization_leaders.each{|ol| people_to_update << ol }
    people_to_update.each { |u| ApplicationMailer.application_created(u, @application, access_token).deliver }
  end

  def redirect_to_finish_wizard(options = nil)
    redirect_to application_path(@application), notice: "Thank you for applying. A governor will update you with your date of vote soon."
  end
end
