class UsersController < ApplicationController
  before_filter :authenticate_user!

  def show
    @user = User.find(params[:id])
    authorize! :show, @user
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    # If we're missing info, load it from WVU's public info
    if @user.fname.nil? || @user.lname.nil? || @user.email.nil?
      url = "#{APP_CONFIG["user_lookup_service"]}/#{@user.username}"
      response = HTTParty.get(url, headers: {"Accept" => "application/json"})
      @remote_user_data = response.parsed_response
      @user.fname = @remote_user_data["firstName"] if @user.fname.nil?
      @user.lname = @remote_user_data["lastName"] if @user.lname.nil?
      @user.email = @remote_user_data["email"] if @user.email.nil?
      @user.save
    end
    authorize! :update, @user
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    authorize! :update, @user
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: t('users.updated') }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
