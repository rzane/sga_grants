class OrganizationsController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index, :names]
  before_filter :notify_incomplete_apps
  before_filter :ensure_category_selected!, only: :create_survey

  def index
    authorize! :index, Organization
    @organizations = Organization.joins(:primary_category).includes(:primary_category)
    @organizations = @organizations.search(params[:search]) if params[:search]
    @organizations = @organizations.grouped_by_category
    respond_to do |format|
      format.html
      format.js
      format.json { render json: @organizations }
    end
  end

  def names
    @organization_names = Organization.pluck(:name)
    render json: @organization_names
  end

  def survey
    @categories = Category.joins(:organizations).includes(:organizations)
    render layout: "university"
  end

  def survey_show
    @organization = Organization.include_categories_and_leaders.find(params[:id])
    render layout: false
  end

  def create_survey
    @organizations = Organization.includes(:organization_leaders).find(params[:category][:organization_ids])
    @organizations.each do |org|
      org.organization_leaders.each do |ol|
        OrganizationMailer.survey_submitted(org, ol, current_user).deliver
      end
    end
    message = t 'organizations.interest_notification', names: @organizations.map(&:name).join(", ")
    redirect_to survey_root_path, notice: message
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    @organization = Organization.include_categories_and_leaders
                                .includes(:fundraising_histories)
                                .find(params[:id])
                                
    authorize! :show, @organization
    respond_to do |format|
      format.html {}
      format.js
      format.json { render json: @organization }
    end
  end

  # GET /organizations/new
  # GET /organizations/new.json
  def new
    authorize! :create, Organization
    @organization = Organization.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @organization }
    end
  end

  # GET /organizations/1/edit
  def edit
    @organization = Organization.find(params[:id])
    session[:return_to] ||= request.referer
    # @organization.fundraising_histories.build
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(params[:organization])
    authorize! :create, Organization
    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: t('organizations.created') }
        format.json { render json: @organization, status: :created, location: @organization }
      else
        format.html { render action: "new" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.json
  def update
    @organization = Organization.find(params[:id])
    authorize! :update, Organization
    params[:organization][:updated_by] = current_user
    respond_to do |format|
      if @organization.update_attributes(params[:organization])
        format.html { redirect_to @organization, notice: t('organizations.updated', name: @organization.name.possessive ) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization = Organization.find(params[:id])
    @organization.destroy
    authorize! :destroy, Organization
    respond_to do |format|
      format.html { redirect_to organizations_url }
      format.json { head :no_content }
    end
  end

  private

  def ensure_category_selected!
    if params[:category].nil?
      redirect_to survey_root_path, alert: t('organizations.must_select_one')
    end
  end
end
