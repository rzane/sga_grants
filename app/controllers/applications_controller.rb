class ApplicationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :validate_access_token, only: :verify
  before_filter :notify_incomplete_apps

  # GET /applications
  # GET /applications.json
  def index
    authorize! :index, Application
    @statuses = %w{pending archived incomplete}
    respond_to do |format|
      format.html {}
      format.json { render json: ApplicationsDatatable.new(view_context) }
    end
  end

  def help
  end

  def finance_report
    authorize! :finance_report, Application
    @total_grant_capacity = Setting.find_by_property("total_sga_funds").try(:value).try(:to_i)
    if @total_grant_capacity.nil?
      return redirect_to root_path, flash: {error: t('errors.grant_cap_not_set')}
    end
    @applications_final_amount_sum = Application.final_amount_sum

    @remaining_funds = @total_grant_capacity - @applications_final_amount_sum
    @number_at_five_hundred = (@total_grant_capacity / 500).to_i
    @amount_can_give = @total_grant_capacity / @number_at_five_hundred
  end

  # GET /applications/1
  # GET /applications/1.json
  def show
    @application = Application.include_all_associations.find(params[:id])
    authorize! :show, @application
    @organization = @application.organization
    respond_to do |format|
      format.html { notify_unverified! unless @application.verified? }
      format.json { render json: @application }
    end
  end

  def new
    return redirect_to organizations_path unless params[:organization_id]
    authorize! :create, Application
    @application = Application.new
    @application.organization = Organization.find(params[:organization_id])
    return invalid_organization!(@application.organization) unless @application.organization.valid?
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @application }
    end
  end

  def verify
    @application = Application.find(params[:id])
    @organization_leader = @application.organization.organization_leaders.find(params[:org_leader_id])
    @application.update_attributes(verified: true, verified_by: @organization_leader)
    redirect_to root_path, flash: {success: t('application.verified') }
  end

  # GET /applications/1/edit
  def edit
    @application = Application.find(params[:id])
    authorize! :update, @application
    @application.build_event unless @application.event
    @application.event.build_transportation unless @application.event.transportation
    @application.event.build_hotel unless @application.event.hotel
    @application.expenditures.build
  end

  def governor_update
    @application = Application.find(params[:id])
    authorize! :governor_update, @application
    respond_to do |format|
      if @application.update_and_notify(params[:application])
        format.html { redirect_to application_path(@application) }
        format.json { render json: view_context.app_updated_response(@application) }
      else
        format.html {redirect_to application_path(@application), alert: "An error occurred!"}
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /applications
  # POST /applications.json
  def create
    @application = find_or_create_from_session
    @application.applicant = current_user

    authorize! :create, Application
    respond_to do |format|
      if @application.save
        format.html do
          session[:app_id] = @application.id
          redirect_to application_steps_path(application_id: @application.id)
        end
        format.json { render json: @application, status: :created, location: @application }
      else
        format.html { render action: "new" }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /applications/1
  # PUT /applications/1.json
  def update
    @application = Application.find(params[:id])
    authorize! :update, @application
    respond_to do |format|
      if @application.update_attributes(params[:application])
        format.html { redirect_to application_steps_path(application_id: @application.id) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1
  # DELETE /applications/1.json
  def destroy
    @application = Application.find(params[:id])
    @application.destroy
    authorize! :destroy, Application
    respond_to do |format|
      format.html { redirect_to applications_url }
      format.json { head :no_content }
    end
  end

  private

  def find_or_create_from_session
    if session[:app_id]
      application = Application.find(session[:app_id].to_i)
      application.assign_attributes(params[:application])
      application
    else
      Application.new(params[:application])
    end
  end

  def invalid_organization!(org)
    message = {info: t('organizations.invalid', org_name: org.name.possessive)}
    redirect_to edit_organization_path(org), flash: message
  end

  def notify_unverified!
    message_for = can?(:view_all, Application) ? 'member' : 'applicant'
    flash.now[:info] =  t("applications.unverified_app_#{message_for}")
  end

  def active_tab
    locations_in_url = @statuses.map{|s| request.fullpath.rindex(s) || 0 }
    return @statuses[0] if locations_in_url.all? {|l| l == 0 }
    index_of_last_status_param = locations_in_url.rindex(locations_in_url.max)
    @statuses[index_of_last_status_param]
  end

  def validate_access_token
    api_key = ApiKey.find_by_access_token(params[:access_token])
    redirect_to root_path, flash: {alert: t('errors.invalid_access_token')} unless api_key
  end
end
