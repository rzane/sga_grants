$ ->
  reset_search = ->
    $('.search-button').removeClass('clear_search').val('Go')
    $('.search').val("")
    $('.search').typeahead('setQuery', '')
    $('#results').fadeOut 'slow', ->
      $(this).children().remove()
      $('#main_orgs_list').fadeIn "slow"

  bind_masonry = ->
    $(".organizations_wrapper").masonry
      itemSelector: ".category:visible"

  $('#accept_terms').on "change", ->
    proceed = $('#proceed_with_application')
    if this.checked
      proceed.removeClass('disabled')
    else
      proceed.addClass('disabled')

  search = $('.search')

  # Index page
  if search.length > 0
    bind_masonry()

    $(window).resize ->
      $(".organizations_wrapper").masonry('reload')

    $('#search_form').on "submit", (e)->
      $('.search').blur()
      if $('.search-button').hasClass('clear_search')
        reset_search()
        return false
    .on "ajax:beforeSend", ->
      $('#main_orgs_list').fadeOut('slow')
      $('.search-button').addClass('clear_search').val('X')

    search.on "keyup", ->
      reset_search() if $(this).val() == ""

    search.typeahead                              
      name: 'organizations'
      prefetch: search.data('source')
      limit: 10
    .on "typeahead:selected typeahead:autocompleted", ->
      $('.search-button').removeClass('clear_search').val('Go')
      $('#search_form').submit()