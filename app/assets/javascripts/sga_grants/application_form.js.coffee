$ ->
  $('#application_vote_result').off("change").on "change", ->
    finalamt = $('.final_amt_contain')
    if $(this).val() is "approved"
      finalamt.show()
    else
      finalamt.hide()

  $("#gov_form")
    .off("ajax:success").on "ajax:success", (data, status, xhr) ->
      $('#gov_update').modal('toggle')
      $('.gov-update-alert').fadeOut ()->
        $(this).text('Application updated successfully')
        $(this).removeClass('alert-info').addClass('alert-success')
        $(this).fadeIn('slow')
      $('#application.tab-pane').html(status.partial)
    .off("ajax:error").on 'ajax:error', (xhr, status, error) ->
      response = $.parseJSON(status.responseText)
      $.each response, (key, value) ->
        el = $('#application_' + key)
        el.closest('.form-group').addClass('has-error')
        el.closest('.col-lg-10').append('<span class="help-block">'+value[0]+'</span>')


  if $(".grant_app").length > 0
    changeAddButtonText = (slider_open, button, question, container) ->
      if slider_open
        button.text("Add " + button.data("name")).removeClass "btn-danger open"
        container.slideUp 400, ->
          question.show 400
      else
        question.hide 400, ->
          container.slideDown 400, ->
            button.text("Remove " + button.data("name")).addClass "btn-danger open"

    toggleSliderContainer = (add_button) ->
      container = $(add_button.data("container"))
      destroy = $(add_button.data("destroy"))
      slider_open = add_button.hasClass("open")
      destroy.prop "checked", slider_open
      question = add_button.parent().find(".question")
      changeAddButtonText slider_open, add_button, question, container

    check_if_slider_should_be_open = ->
      slider_button = $('.slider_button')
      container = $(slider_button.data('container'))
      destroy = $(slider_button.data('destroy'))
      if slider_button.hasClass('open') then container.show()

    showOrHideRemoveIcon = ->
      remove_icons = $(".expenditures .remove_fields")
      there_is_more_than_one = $(".expenditures .expenditure_inputs").length > 1
      if there_is_more_than_one then remove_icons.show() else remove_icons.hide()

    initialize = ->
      check_if_slider_should_be_open()
      showOrHideRemoveIcon()

    initialize()

    $("#add_hotel, #add_transportation").off("click").on "click", (e) ->
      toggleSliderContainer $(e.target)
      return false;
    
    $(".expenditures").on "cocoon:before-insert cocoon:after-insert cocoon:after-remove", (e, insertedExpenditure) ->
      showOrHideRemoveIcon()