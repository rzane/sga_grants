$ ->
  bind_dynatable_events = (table) ->
    table.on 'dynatable:init', (e, dynatable) ->
      tab = $(this).closest('.tab-pane')
      tab.find('.dynatable-search, .dynatable-per-page').wrapAll('<div class="row dynatable-top" />')
      tab.find('.table-dynatable').wrap('<div class="row dynatable-middle"/>')
      tab.find('.dynatable-top, .dynatable-middle, .dynatable-bottom').wrapAll('<div class="dynatable-container" />')
      tab.find('.dynatable-container').find('input, select').addClass('form-control input-sm');
      tab.find('.dynatable-per-page, .dynatable-search').addClass('col-xs-6')
    .on 'dynatable:beforeProcess', ->
      NProgress.start()
    .on 'dynatable:afterProcess', ->
      NProgress.done()
      NProgress.remove()

  initialize_dynatable = (table)->
    bind_dynatable_events table
    tab = table.closest('.tab-pane')
    table.dynatable
      features:
        pushState: false
      table:
        copyHeaderClass: true
      inputs:
        paginationClass: 'pagination dynatable-pagination pagination-sm'
        paginationActiveClass: 'active'
        paginationDisabledClass: 'disabled'
        paginationNext: "»"
        paginationPrev: "«"
        processingText: ''
      dataset:
        ajax: true
        ajaxUrl: table.data('url')
        ajaxOnLoad: true
        records: []

  initialize_dynatable $('#pending_table')
  
  $('#archived_tab').one 'click', ->
    initialize_dynatable $('#archived_table')

  $('#incomplete_tab').one 'click', ->
    initialize_dynatable $('#incomplete_table')