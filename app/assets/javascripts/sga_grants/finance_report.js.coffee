$ ->
  num_grants_input = $('.number_of_grants')
  amount_can_give = $('.amount_can_give')
  remaining = num_grants_input.data('remaining')

  num_grants_input.on "change, keyup", ->
    num_grants = $(this).val()
    amt_per_grant = (remaining/num_grants).toFixed(2)
    amount_can_give.val(amt_per_grant)

  amount_can_give.on "change, keyup", ->
    amt_per_grant = $(this).val()
    number_of_grants = (remaining/amt_per_grant).toFixed(0)
    num_grants_input.val(number_of_grants)
    console.log(num_grants_input.val())
