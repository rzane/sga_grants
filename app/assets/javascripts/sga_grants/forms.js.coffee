$ ->
  $(".fix_long_textareas textarea").off("focus").on "focus", ->
    $(this).animate height: 150, "normal"

  $("input[type=number]").keydown (event) ->
    keycodes_to_ignore = [46, 8, 9, 27, 13, 190]
    if ($.inArray(event.keyCode, keycodes_to_ignore) > -1) or (event.keyCode is 65 and event.ctrlKey is true) or (event.keyCode >= 35 and event.keyCode <= 39)
      return
    else
      event.preventDefault()  if event.shiftKey or (event.keyCode < 48 or event.keyCode > 57) and (event.keyCode < 96 or event.keyCode > 105)
