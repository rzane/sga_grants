#
#= require jquery
#= require jquery_ujs
#= require jquery.turbolinks
#= require turbolinks
#= require cocoon
#= require masonry/jquery.masonry
#= require flatly/loader
#= require jquery.ui.all
#= require jquery.ui.datepicker
#= require twitter/typeahead.min
#= require dynatable/jquery.dynatable
#= require nprogress/nprogress
#= require_self
#= require_tree ./sga_grants
#

$(document).on "page:fetch", ->
  NProgress.start()

$(document).on "page:change", ->
  NProgress.done()

$(document).on "page:restore", ->
  NProgress.remove()
