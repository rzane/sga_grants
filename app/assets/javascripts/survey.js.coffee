#
#= require jquery
#= require_self
#= require jquery_ujs
#= require foundation
#

$ ->
  $(document).foundation()

  $('.category-results').hide()
  $('.selected').hide()

  $('#category_id').on "change", ->
    $('#category_select_form').submit()
  .one "change", ->
    $('.category-results').show()

  $(document).on "click", '.surveypopup', ->
    $('#modal').foundation('reveal', 'open', {
        url: $(this).data('source'),
        # success: (data) ->
        error: ->
          $('#modal').append($('p').text('An error occurred!'))
    })