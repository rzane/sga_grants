class TimepickerInput < SimpleForm::Inputs::Base
  def input
    # input_html_options[:class] << "form-control"
    input_html_options[:type] = "time"
    input_html_options[:class] << "form-control"
    @builder.text_field(attribute_name, input_html_options)
  end
end
