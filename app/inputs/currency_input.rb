class CurrencyInput < SimpleForm::Inputs::Base
  def input
    input_html_options[:class] << "form-control"
    input_html_options[:step] = "0.01"
    "<div class='input-group'><span class='input-group-addon'>$</span>#{@builder.number_field(attribute_name, input_html_options)}</div>".html_safe
  end
end
