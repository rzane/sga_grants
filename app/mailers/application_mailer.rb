class ApplicationMailer < ActionMailer::Base
  default from: Proc.new{ APP_CONFIG["grants_sender"] }, reply_to: Proc.new{ APP_CONFIG["grants_sender"] }
  
	def application_created(user, application, access_token)
		@user = user
		@application = application
		@is_applicant = application.applicant == user 
		@applicant_pronoun = @is_applicant ? "You" : application.applicant.fullname
	  @verify_link = verify_application_url(@application, :org_leader_id => @user.id, :access_token => access_token)
		mail(:to => "#{user.fullname} <#{user.email}>", :subject => "SGA Grant Application Confirmation")
	end

  def date_of_vote_assigned(user, application)
    @user = user
    @application = application
    mail(:to => "#{user.fullname} <#{user.email}>", :subject => "SGA Grant Voting Date")
  end

  def application_approved(user, application)
    @user = user
    @application = application
    mail(:to => "#{user.fullname} <#{user.email}>", :subject => "SGA Grant Approved")
  end

end
