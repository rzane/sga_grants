class OrganizationMailer < ActionMailer::Base
  default from: Proc.new{ APP_CONFIG["survey_sender"] }, reply_to: Proc.new{ APP_CONFIG["survey_sender"] }

  def survey_submitted(organization, organization_leader, user)
    @org_leader = organization_leader
    @organization = organization
    @user = user
    mail(:to => "#{organization_leader.fullname} <#{organization_leader.email}>", :subject => "#{user.fullname} is interested in #{organization.name}")
  end
end
