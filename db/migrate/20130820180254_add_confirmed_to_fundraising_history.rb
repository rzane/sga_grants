class AddConfirmedToFundraisingHistory < ActiveRecord::Migration
  def change
    add_column :fundraising_histories, :confirmed, :boolean
  end
end
