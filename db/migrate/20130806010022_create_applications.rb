class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.integer :organization_id
      t.integer :applicant_id
      t.date :date_of_vote
      t.string :vote_result
      t.decimal :amt_requested
      t.date :funds_needed_by
      t.text :reason_for_funds
      t.text :impact_on_univ
      t.text :additional_info
      t.boolean :complete
      t.timestamps
    end
  end
end
