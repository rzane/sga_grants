class CreateCategoryOrganizationsTable < ActiveRecord::Migration
  def change
    create_table :category_organizations do |t|
      t.references :category
      t.references :organization
    end

    add_index :category_organizations, [:category_id, :organization_id], :name => 'cat_org_catid_orgid'

    add_index :category_organizations, :organization_id, :name => 'cat_org_org_id'
  end
end
