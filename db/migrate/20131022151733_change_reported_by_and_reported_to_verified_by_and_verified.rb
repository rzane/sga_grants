class ChangeReportedByAndReportedToVerifiedByAndVerified < ActiveRecord::Migration
  def change
    remove_column :applications, :reported_as_spam
    remove_column :applications, :reported_by
    add_column :applications, :verified, :boolean
    add_column :applications, :verified_by, :integer
  end
end
