class AddLocationFieldsToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :city, :string
    add_column :hotels, :state, :string
    add_column :hotels, :number_of_rooms, :integer
		add_column :hotels, :additional_info, :text
  end
end
