class AddFinalAmountToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :final_amount, :decimal
  end
end
