class AddTransportationTypeIdToTransportation < ActiveRecord::Migration
  def change
    add_column :transportations, :transportation_type_id, :integer
  end
end
