class AddPrimaryCategoryToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :primary_category_id, :integer
  end
end
