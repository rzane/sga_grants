class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :meeting_day_of_week
      t.time :meeting_time
      t.integer :meeting_frequency
      t.text :description
      t.text :purpose
      t.integer :number_of_members
      t.string :website
      t.timestamps
    end
  end
end
