class CreateTransportations < ActiveRecord::Migration
  def change
    create_table :transportations do |t|
      t.integer :event_id
      t.string :type
      t.decimal :cost
      t.string :company_name
      t.string :phone
      t.string :website
      t.text :additional_info

      t.timestamps
    end
  end
end
