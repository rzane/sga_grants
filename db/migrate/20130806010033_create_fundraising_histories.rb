class CreateFundraisingHistories < ActiveRecord::Migration
  def change
    create_table :fundraising_histories do |t|
      t.integer :organization_id
      t.string :name
      t.decimal :amount_received
			t.timestamps
    end
  end
end
