class AddUpdatedByToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :updated_by, :integer
  end
end
