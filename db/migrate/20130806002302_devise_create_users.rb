class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
			t.string :email
      t.string :fname
      t.string :lname
      t.string :username, :null => false
		end
    add_index :users, :username, :unique => true
  end
end
