class CreateTransportationTypes < ActiveRecord::Migration
  def change
    create_table :transportation_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
