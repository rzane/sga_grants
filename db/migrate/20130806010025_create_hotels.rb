class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.integer :event_id
      t.string :name
      t.string :phone
      t.integer :people_per_room
      t.decimal :cost_per_room

      t.timestamps
    end
  end
end
