class ChangeVerifiedByToVerifiedById < ActiveRecord::Migration
  def change
    rename_column :applications, :verified_by, :verified_by_id
  end
end
