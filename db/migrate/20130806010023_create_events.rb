class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :application_id
      t.string :place_of_event
      t.date :start_date
      t.date :end_date
      t.integer :number_attending

      t.timestamps
    end
  end
end
