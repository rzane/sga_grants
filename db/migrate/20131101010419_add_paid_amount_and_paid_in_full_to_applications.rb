class AddPaidAmountAndPaidInFullToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :paid_amount, :decimal
    add_column :applications, :paid_in_full, :boolean
  end
end
