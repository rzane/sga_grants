class MakeRequestedAmountACalculatedField < ActiveRecord::Migration
  def change
		remove_column(:applications, :amt_requested)
  end
end
