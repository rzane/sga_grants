class AddReportedAsSpamToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :reported_as_spam, :boolean
		add_column :applications, :reported_by, :integer
  end
end
