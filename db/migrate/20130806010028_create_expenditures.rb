class CreateExpenditures < ActiveRecord::Migration
  def change
    create_table :expenditures do |t|
      t.integer :application_id
      t.integer :expenditure_type_id
      t.text :description
      t.decimal :cost

      t.timestamps
    end
  end
end
