class AddDateToFundraisingHistory < ActiveRecord::Migration
  def change
    add_column :fundraising_histories, :date, :date
  end
end
