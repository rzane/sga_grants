class CreateOrganizationLeaders < ActiveRecord::Migration
  def change
    create_table :organization_leaders do |t|
      t.string :fname
      t.string :lname
      t.string :email
      t.string :title
      t.integer :organization_id
      t.timestamps
    end
  end
end
