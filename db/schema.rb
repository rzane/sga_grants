# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131101010419) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "api_keys", force: true do |t|
    t.string "access_token"
  end

  create_table "applications", force: true do |t|
    t.integer  "organization_id"
    t.integer  "applicant_id"
    t.date     "date_of_vote"
    t.string   "vote_result"
    t.date     "funds_needed_by"
    t.text     "reason_for_funds"
    t.text     "impact_on_univ"
    t.text     "additional_info"
    t.boolean  "complete"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "final_amount"
    t.boolean  "verified"
    t.integer  "verified_by_id"
    t.decimal  "paid_amount"
    t.boolean  "paid_in_full"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_organizations", force: true do |t|
    t.integer "category_id"
    t.integer "organization_id"
  end

  add_index "category_organizations", ["category_id", "organization_id"], name: "cat_org_catid_orgid"
  add_index "category_organizations", ["organization_id"], name: "cat_org_org_id"

  create_table "events", force: true do |t|
    t.integer  "application_id"
    t.string   "place_of_event"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "number_attending"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "expenditure_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "expenditures", force: true do |t|
    t.integer  "application_id"
    t.integer  "expenditure_type_id"
    t.text     "description"
    t.decimal  "cost"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fundraising_histories", force: true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.decimal  "amount_received"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "confirmed"
    t.date     "date"
  end

  create_table "hotels", force: true do |t|
    t.integer  "event_id"
    t.string   "name"
    t.string   "phone"
    t.integer  "people_per_room"
    t.decimal  "cost_per_room"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
    t.string   "state"
    t.integer  "number_of_rooms"
    t.text     "additional_info"
  end

  create_table "organization_leaders", force: true do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "email"
    t.string   "title"
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organization_users", force: true do |t|
    t.integer  "organization_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.string   "meeting_day_of_week"
    t.time     "meeting_time"
    t.integer  "meeting_frequency"
    t.text     "description"
    t.text     "purpose"
    t.integer  "number_of_members"
    t.string   "website"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "primary_category_id"
    t.integer  "updated_by"
  end

  create_table "settings", force: true do |t|
    t.string   "property"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transportation_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transportations", force: true do |t|
    t.integer  "event_id"
    t.string   "type"
    t.decimal  "cost"
    t.string   "company_name"
    t.string   "phone"
    t.string   "website"
    t.text     "additional_info"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "transportation_type_id"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "fname"
    t.string   "lname"
    t.string   "username",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
  end

  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
